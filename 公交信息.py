import os.path
import re
import requests
from bs4 import BeautifulSoup

bus_routes = []

with open('route.txt', 'r', encoding='utf-8') as file:
    links = file.read().splitlines()
    for item in links:
        title = item.split(' ')[0]
        url = item.split(' ')[1]
        dic = {'title': f'{title}', 'url': f'{url}'}
        bus_routes.append(dic)
info = []
for item in bus_routes:
    url = item['url']
    response = requests.get(url)
    soup = BeautifulSoup(response.text, 'html.parser')
    bus_desc = soup.find('ul', class_='bus-desc')
    dic = {'运行路线': item['title']}
    bus_time = list(bus_desc)[0].get_text()
    bus_price = list(bus_desc)[1].get_text()
    bus_company = list(bus_desc)[2].get_text()
    time = re.split(r'[： |]', bus_time)
    price = re.split(r'：', bus_price)
    company = re.split(r'：', bus_company)
    if len(time) == 5:
        dic[f'{time[1]}运行时间'] = time[2]
        dic[f'{time[3]}运行时间'] = time[4]
    elif len(time) == 4:
        dic[f'{time[1]}运行时间'] = time[2] + ' ' + time[3]
    elif len(time) == 3:
        dic[f'开出运行时间'] = time[1]
        dic[f'回来运行时间'] = time[2]
    dic['参考票价'] = price[1]
    dic['运行公司'] = company[1]
    station_go = []
    station_return = []
    bus_station = soup.find_all('div', class_='bus-lzlist mb15')
    count = 0

    for item in bus_station:
        count += 1
        aria = item.find_all('a', {'aria-label': True})
        for a in aria:
            aria_label = a['aria-label']
            if count == 1:
                station_go.append(aria_label)
            if count == 2:
                station_return.append(aria_label)
    for item in station_go:
        print(item)
    for item in station_return:
        print(item)
    if len(station_return) == 0:
        dic['是否为循环路线'] = '是'
    else:
        dic['是否为循环路线'] = '否'
    print(dic)
    output_folder = './公交信息'
    file_name = os.path.join(output_folder, f"{dic['运行路线']}.txt")
    with open(file_name, 'w', encoding='utf-8') as file:
        for key, value in dic.items():
            file.write(f'{key}:{value}\n')
        file.write("出发:")
        for item in station_go:
            file.write(f"{item} ")
        file.write('\n')
        if station_return:
            file.write('返回:')
            for item in station_return:
                file.write(f'{item} ')
