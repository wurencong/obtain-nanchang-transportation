运行路线:南昌169路公交车路线
参考票价:二元一票制；洪城通可用
运行公司:南昌公交集团五公司
是否为循环路线:否
出发:1 湾里旅游集散中心站点 2 东方红站点 3 翠园站点 4 湾里区妇保所站点 5 岭秀湖市民公园东站点 6 湾里大道西口站点 7 火盘新村站点 8 工业环路口站点 9 禹港村站点 10 湾里大道东口站点 11 小桥站点 12 文全村站点 13 新建区开发区站点 14 工业四路口(工业大道工业四路口)站点 15 工业三路口站点 16 工业二路口站点 17 工业大道东口站点 18 北郊站点 19 新建区血防站站点 20 兴国路站点 21 开关厂(长征西路兴国路口)站点 22 长征西路口·新建中心商业广场站点 23 长堎站点 24 丰和立交站点 25 抚生路口站点 26 洪城大市场站点 27 武警总队站点 28 司马庙立交北站点 29 绳金塔(新中原大剧院)站点 30 公交保育院·南昌古玩城站点 31 老福山西站点 32 火车站(西广场北公交站)站点 
返回:1 火车站(西广场北公交站)站点 2 老福山西站点 3 公交保育院·南昌古玩城站点 4 绳金塔(站前西路前进路口)站点 5 司马庙立交北站点 6 武警总队站点 7 洪城大市场站点 8 抚生路口站点 9 丰和立交站点 10 长堎站点 11 长征西路口·新建中心商业广场站点 12 子实路站点 13 子实路兴国路口[招呼站]站点 14 新建区六中站点 15 工业大道东口站点 16 工业二路口站点 17 工业三路口站点 18 工业四路口(工业大道工业四路口)站点 19 新建区开发区站点 20 文全村站点 21 小桥站点 22 湾里大道东口站点 23 湾里水司站点 24 师大湾里附小学站点 25 火盘新村站点 26 湾里大道西口站点 27 岭秀湖市民公园东站点 28 湾里行政中心站点 29 翠园站点 30 东方红站点 31 湾里旅游集散中心站点 