运行路线:南昌568路公交车路线
开出运行时间:铁河8:15-13:55
回来运行时间:金桥乡8:50-14:20
参考票价:二元一票制；洪城通可用
运行公司:南昌公交集团六公司
是否为循环路线:否
出发:1 铁河站点 2 铁河敬老院站点 3 海昏侯工作站站点 4 联合村站点 5 三岔河口站点 6 长胜站点 7 汪山土库站点 8 彭家站点 9 大塘街上站点 10 大塘站点 11 金桥乡站点 
返回:1 金桥乡站点 2 大塘站点 3 大塘街上站点 4 彭家站点 5 汪山土库站点 6 长胜站点 7 三岔河口站点 8 联合村站点 9 海昏侯工作站站点 10 铁河敬老院站点 11 铁河站点 