import requests
from bs4 import BeautifulSoup  # 获取<div class='list'>内所以超链接

url = "https://nanchang.8684.cn/"
# 使用Beautiful Soup解析HTML代码
response = requests.get(url)
soup = BeautifulSoup(response.text, 'html.parser')
# 查找包含链接的<div class="list">元素
div_bus = soup.find('div', class_='bus-layer depth w120')
list = []
for link in div_bus.find_all():
    if link.get('href'):
        href = link.get('href')
        full_link = url + href
        list.append(full_link)
for item in list:
    print(item)
# 将链接写入文件
with open('links.txt', 'w', encoding='utf-8') as file:
    for link in list:
        file.write(link + '\n')
