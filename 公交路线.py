import requests
from bs4 import BeautifulSoup

with open('links.txt', 'r', encoding='utf-8') as file:
    links = file.read().splitlines()
bus_routes = []
for link in links:
    response = requests.get(link)
    # 使用Beautiful Soup解析页面内容
    soup = BeautifulSoup(response.text, 'html.parser')
    pre = "https://nanchang.8684.cn/"
    # 查找包含公交路线超链接的<div class="list clearfix">元素
    div_list = soup.find('div', class_='list clearfix')
    for item in div_list:
        href = item.get('href')
        title = item.get('title')
        url = pre + href
        dic = {'title': f'{title}', 'url': f'{url}'}
        print(dic['title'], dic['url'])
        bus_routes.append(dic)
    with open('route.txt', 'w', encoding='utf-8') as file:
        for item in bus_routes:
                file.write(item['title'] + ' ' + item['url'] + '\n')
